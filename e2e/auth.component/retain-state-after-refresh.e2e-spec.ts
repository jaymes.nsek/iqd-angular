import {browser, by, element} from "protractor";
import {makeUrl, urlReset, urlSignIn} from "../helper";

let queryUrl: string


describe('url in browser', () => {
  beforeAll(async () => {
    queryUrl = makeUrl('/auth/sign-in')
    await browser.driver.get(queryUrl)
  })

  it('should reflect state (UI) views after refresh event', () => {
    // Start off at /sign-in
    expect(browser.getCurrentUrl()).toBe(urlSignIn)

    // Navigate to /reset
    let title = element(by.id('header-title'))
    element(by.css('button[id="reset-pw-btn"]')).click()
    expect(title.getText()).toBe('Reset Password')
    expect(browser.getCurrentUrl()).toEqual(urlReset)

    // Refresh page
    browser.driver.navigate().refresh()

    // validate that UI state remains aligned with url
    // NB: Only title is checked as others are already confirmed in AuthComp Integration tests
    title = element(by.id('header-title'))
    expect(browser.getCurrentUrl()).toEqual(urlReset)
    expect(title.getText()).toBe('Reset Password')
  })
})
