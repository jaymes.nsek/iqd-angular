import {browser, by, element} from "protractor"
import {makeUrl} from "../helper";

let queryUrl: string
let title


describe('Navigation between states', () => {
  beforeAll(async () => {
    queryUrl = makeUrl('/auth/sign-in')
    await browser.driver.get(queryUrl)
  })

  it('should be SIGN_IN', async () => {
    const url = await browser.driver.wait(async () => {
      return await browser.driver.getCurrentUrl()
    }, 3000)

    // Start off at /sign-in
    expect(url).toEqual(queryUrl)
  })

  it('should be "/register" after navigation from "/sign-in"', () => {
    // Initially in sign-in state from the beforeAll call
    title = element(by.id('header-title'))
    expect(title.getText()).toBe('Sign In')

    // Navigate from /sign-in to /register
    element(by.css('a[class="footer-nav"]')).click()
    expect(title.getText()).toBe('Create Account')
  })

  it('should be "/sign-in" after navigation from "/register"', () => {
    // In register state from preceding test
    title = element(by.id('header-title'))
    expect(title.getText()).toBe('Create Account')

    // transition back to sign-in
    element(by.css('a[class="footer-nav"]')).click()
    expect(title.getText()).toBe('Sign In')
  })

  it('should be "/reset" after navigation from "/sign-in"', () => {
    // in sign-in state from preceding test
    title = element(by.id('header-title'))
    expect(title.getText()).toBe('Sign In')

    // transition to reset
    element(by.css('button[id="reset-pw-btn"]')).click()
    expect(title.getText()).toBe('Reset Password')
  })
})
