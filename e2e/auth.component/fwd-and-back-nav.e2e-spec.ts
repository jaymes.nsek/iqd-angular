import {makeUrl, urlCreate, urlReset, urlSignIn} from "../helper";
import {browser, by, element, ElementFinder} from "protractor";


describe('url in browser', () => {
  let queryUrl: string
  let title: ElementFinder

  function verifySignIn() {
    // Matches url and UI
    expect(title.getText()).toBe('Sign In')
    expect(browser.getCurrentUrl()).toEqual(urlSignIn)
  }

  function verifyRegister() {
    expect(title.getText()).toBe('Create Account')
    expect(browser.getCurrentUrl()).toEqual(urlCreate)
  }

  function verifyReset() {
    // Matches url and UI
    expect(title.getText()).toBe('Reset Password')
    expect(browser.getCurrentUrl()).toEqual(urlReset)
  }

  function itSignIn() {
    it('<< back: should return url to /sign-in',  () => {
      browser.driver.navigate().back()
      verifySignIn()
    })
  }

  beforeAll(async () => {
    queryUrl = makeUrl('/auth/sign-in')
    await browser.driver.get(queryUrl)

    title = element(by.id('header-title'))
  })

  it('test setup: should transition through the different states', () => {
    // Start off at /sign-in
    expect(browser.getCurrentUrl()).toBe(urlSignIn)

    // /sign-in to /register
    element(by.css('a[class="footer-nav"]')).click()
    verifyRegister()

    // /register back to /sign-in
    element(by.css('a[class="footer-nav"]')).click()
    verifySignIn()

    // /sign-in to /reset
    element(by.css('button[id="reset-pw-btn"]')).click()
    verifyReset()
  })

  itSignIn()

  it('forward >>: should return url to /reset',  () => {
    browser.driver.navigate().forward()
    verifyReset()
  })

  itSignIn()

  it('<< back: should return url to /register',  () => {
    browser.driver.navigate().back()
    verifyRegister()
  })

  itSignIn()
})
