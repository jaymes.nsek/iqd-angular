export const urlReset = 'http://localhost:4200/auth/reset'
export const urlSignIn = 'http://localhost:4200/auth/sign-in'
export const urlCreate = 'http://localhost:4200/auth/register'

export function makeUrl(rel: string = '') {
  return 'http://localhost:4200' + rel
}
