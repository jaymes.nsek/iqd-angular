import {Component, Input, OnInit} from "@angular/core";
import {TabItem} from "../interfaces";
import {MatIconRegistry} from "@angular/material/icon";
import {DomSanitizer} from "@angular/platform-browser";

const PAUSE_ICON =
  `
  <svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#000">
    <path d="M0 0h24v24H0z" fill="none"/><path d="M6 19h4V5H6v14zm8-14v14h4V5h-4z"/>
  </svg>
`

const PLAY_ICON =
  `
  <svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#000">
    <path d="M0 0h24v24H0z" fill="none"/><path d="M8 5v14l11-7z"/>
  </svg>
`

@Component({
  selector: 'app-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.css']
})
export class TabComponent implements OnInit {
  minHeight = 0 // tracks clearance height; to ensure that height does not change as tabs are toggled

  intervalId: number | null = null

  play: boolean | null = null
  // Used to track pause/play directive from the UI, when true, the auto transition/animation proceeds
  // as normal, otherwise it is only activated by clicking the tabs on UI.
  manualPlay = false

  /**
   * Represents an array of objects as per {@link #TabItem}
   */
  @Input() content: TabItem[] = []

  // Default index is 0, this pattern only works because HTML is activated when (content.length > 0)
  // thus eliminate the need to use ngOnChanges to first register the element after content is init
  selectedItem = 0

  // Used as a lock to limit screen callback onresize
  lock = false


  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
    // Register icons
    iconRegistry.addSvgIconLiteral('pause', sanitizer.bypassSecurityTrustHtml(PAUSE_ICON))
    iconRegistry.addSvgIconLiteral('play', sanitizer.bypassSecurityTrustHtml(PLAY_ICON))
  }


  ngOnInit(): void {
    // Add animation event listeners
    window.onload = (e) => {
      const element = document.getElementById("img-wrapper")

      if (element == null)
        return console.warn("ngOnInit: element == null")

      element.addEventListener("animationend", () => {
        this.play = false
        //console.warn("ngOnInit: animationend")
      })
      //console.warn("ngOnInit: element event set")
    }

    // TODO: An alternative solution: of adding text tags for each TabItem desc and list, thereby
    //  ensuring that all tags will scale up to the tallest.
    //  This solution will also can also adoption of at least two tags for the img, allowing
    //  a roll out and roll in transition effect to be made possible.
    this.setTabSizeListeners()
  }

  setSelectedItem(index: number) {
    this.selectedItem = index | 0
  }

  /**
   * Intended to be used as an intialiser to determine the index that produces the
   * greatest height for the component, the value then flagged as {@link #minHeight};
   *
   * @implNote returning a Promise is necessary to enforce synchronisation, so that call to
   *           {@link #transitionTab} is made only after initialisation has returned.
   */
  async initHeight(): Promise<number> {
    const interval = 20 // ms
    this.selectedItem = 0 // Begin at index 0 if an initialiser call, so that all indices are traversed

    const root = document.getElementById('tab-root')
    if (root == null) {
      console.warn('logHeight: "root" is null!')
      return 0
    }

    return new Promise((resolve) => {
      const heightArr: number[] = []

      this.releaseIntervalId()

      this.intervalId = setInterval(
        () => {
          const index = this.nextIndex()
          this.selectedItem = index

          const height = root.offsetHeight
          heightArr.push(height)  // Call to establish the assumed height of the item at the current index

          // Signals the end of assumed height computation
          // Add additional 10px to clear the tallest (fit) item
          if (index === 0) {
            heightArr.sort((a, b) => b - a) // Sort highest to lowest
            resolve(heightArr[0])
          }
        },
        interval
      );
    })
  }

  /**
   * Calculate Optimal height for {@link TabComponent} on DOMContentLoaded and onresize events.
   *
   * @summary The goal of this pattern is to calculate an optimal height that accommodates all
   *          TabItems such that height remains constant as transition through the tab occurs.
   */
  setTabSizeListeners() {
    // Since in determining height, all items are rapidly set in turn, the views of interest
    // are taken off focus during so as not to appear glitched!
    const setVisibility = (visible: boolean) => {
      const imgWrapper = window.document.getElementById('img-wrapper')
      const txtWrapper = window.document.getElementById('text-wrapper')
      if (imgWrapper == null || txtWrapper == null)
        return console.error("ngOnInit: Elements are null")

      const state = visible ? 'visible' : 'hidden'
      imgWrapper.style.visibility = state
      txtWrapper.style.visibility = state
    }

    let timer: number

    const callback = () => {
      timer = setTimeout(async () => {
        // hide transition tags so that it does not appear glitched as height is determined
        setVisibility(false)

        await this.initHeight()
          .then((minHeight) => {
            minHeight += 20 // Add extra clearance
            this.minHeight = minHeight
            setVisibility(true) // show transition tags
            this.transitionTab(5000)
          })
      })
    }

    window.addEventListener('DOMContentLoaded', () => {
      callback()
    })

    // Reload the page - to recompute TabComponent height minHeight - on window resize
    window.addEventListener('resize', () => {
      // Clear previously triggered timeout (signifying it has not been invoked before the new trigger)
      clearTimeout(timer)

      timer = setTimeout(() => {
        location.reload()
      }, 200)
    })
  }

  /**
   * Takes time interval (in ms) to transition between the tab items, setting selectedItem
   * @param interval the time between calling the handler function of #setInterval
   */
  transitionTab(interval: number) {
    this.releaseIntervalId()

    this.intervalId = setInterval(
      () => {
        // Only traverse the tabs when autoplay is active
        if (!this.manualPlay)
          this.selectedItem = this.nextIndex()

        // Set true to trigger an animated entry on selectedItem (only when manualPlay is false)
        this.play = !this.manualPlay
      },
      interval
    );
  }

  releaseIntervalId() {
    if (this.intervalId != null) {
      clearInterval(this.intervalId);
      this.intervalId = null;
    }
  }

  nextIndex() {
    const next = ++this.selectedItem
    const indexCeiling = this.content.length
    return (next < indexCeiling) ? next : 0 // Returning 0 at the last element creates a circular loop for transition
  }

  /**
   * Toggles boolean {@link #manualPlay} which controls the animation FAB icon as well as whether
   * auto transition/animation proceeds as normal, or is  activated only by clicking the tabs.
   */
  manualControl() {
    this.manualPlay = !this.manualPlay
  }
}
