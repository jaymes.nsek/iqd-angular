/**
 * Return the value of the style property associated with the given parent element.
 *
 * @param rootId The root element's #id, e.g. <div id="myDic"> <span></span> </div>
 * @param property the component's CSS property of interest; "--" should precede a Custom property.
 * @param defVal the default value to return if a TypeError is thrown
 * @return Promise<string>
 */
export async function getStyleAfterDOMLoad(rootId: string, property: string, defVal: string = ''): Promise<string> {
  const callback = () => {
    try {
      const root = (document.getElementById(rootId) as Element)
      return getComputedStyle(root).getPropertyValue(property).trim()
    } catch {
      return defVal
    }
  }

  // Wait for DOMContentLoaded event if already not called
  if (document.readyState === 'complete' || document.readyState === 'interactive')
    return callback()
  else {
    return await new Promise(resolve => {
      window.addEventListener('DOMContentLoaded', () => {
        resolve(callback())
      })
    })
  }
}
