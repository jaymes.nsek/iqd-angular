/**
 * Intended as a way for sub Components to acquire the centrally-update browser's height
 * params - which is done in the app entry point, {@link AppComponent}.
 * @implNote Browser's height = contentHeight + menuHeight
 */
export interface OnBrowserResizeListener {
  onBrowserResize(menuHeight: number, contentHeight: number): void;
}

/**
 * @param desc item's description
 * @param link can be dir relative, or absolute path, as well as an external link; a src link
 *        to a media resource or a CSS href, for example.
 */
export interface LinkItem {
  title: string
  link: string
}

/**
 * @param points is an array of bullet points
 */
export interface TabItem extends LinkItem {
  points: string[]
}

export interface PartnerItem extends LinkItem {
  desc: string
}

export interface NewsItem extends LinkItem {
  date: string
  summary: string
}
