export function createTabItem(title: string, points: string[], src: string) {
  return {title, points, link: src}
}

/**
 * TODO - Dummy content which should be replaced with a call to DB, to allow for dynamic modification.
 */
export function getTabContent() {
  return [
    createTabItem('Engineering Support Services', [
        'Application support for new designs & problem resolution',
        'Sample development for prototypes',
        'Full electrical testing & screening',
        'Crystal test data for electrical modelling by customer (i.e. motional parameters)',
        'Frequency/temperature testing with customer specific ramp-profiles',
        'Product burn-in, heat-soak & accelerated ageing',
        'Full circuit characterisation service',
        'MTIE/TDEV testing'],
      imgSrc('engineering-support-services_500.jpg')),
    createTabItem('Small Low Height SMD Quartz Crystal', [
        '3.2 x 2.5mm hermetically sealed ceramic',
        '4-pad package only 0.8mm high',
        'Automotive version available to AEC-Q200/TS16949 with PPAP levels 1 to 5',
        'Stabilities down to ±10ppm over -40 to +85°C or ±15ppm over -40 to +125°C for automotive applications',
        'Very wide frequency range from 10 to 200MHz',
        'Typical applications: ABS sensors, engine control Units (ECU), ethernet, home automation & entertainment' +
        ', process management systems, smart lighting, tyre pressure monitoring systems (TPMS) and Wi-Fi'],
      imgSrc('small-low-height-smd-quartz-crystal_500.jpg')),
    createTabItem('Smallest 32.768kHz clock oscillator', [
      '1.6 x 1.2 x 0.7mm hermetically sealed ceramic package',
      'Only 30µA and with a standby current as low as 3µA',
      'Delivers better temperature characteristic than standard 32.768kHz tuning fork crystal based oscillators due to the use of an AT-cut crystal normally found in higher frequency oscillators',
      'Start-up time only 7ms at 3.3V with a rise and fall time of 200ns',
      'CMOS output with supply voltage 1.8V',
      'Typical applications: battery management systems, communication modules and systems (Bluetooth, Wi-Fi, Wireless LAN & Zigbee), LCD lighting systems and smart meters (AMR)'
    ], imgSrc('smallest-32-768khz-clock-oscillator_500.jpg')),
    createTabItem('Low Jitter High Frequency VCXO', [
      'Excellent low phase jitter performance between 0.05 to 0.3ps RMS max over 12kHz to 20MHz',
      'Very wide frequency range from 1 to 800MHz',
      'Hermetically sealed 7.0 x 5.0mm ceramic 6-pad package',
      'Choice of three outputs: HCMOS, LVPECL and LVDS',
      'Primarily designed for applications such as DSL/ADSL, ethernet (10G/40G/100G), Femto & Pico cells, SONET/SDH and video broadcasting'
    ], imgSrc('low-jitter-high-frequency-vcxo_500.jpg')),
    createTabItem('Ultra Miniature OCXO only 9.7 x 7.5mm', [
      'Housed in a 4-pad plastic package with a fibre glass base measuring only 9.7 x 7.5mm',
      '±10ppb stability over -20 to +70°C or ±20ppb over -40 to +85°C',
      'Frequency ageing ‹2ppb per day and maximum of 3ppm over a 10 years',
      'Frequency range from 5 to 50MHz',
      'Primarily designed for applications such as Ethernet switches, Femto & Pico cells, Routers, SONET/SDH, Stratum 3 and SyncE & IEEE188',
      'MTIE/TDEV testing available'
    ], imgSrc('ultra-miniature-ocxo-only-9-7-x-7-5mm_500.jpg'))
  ]
}

/**
 * Forms SPA path of an image relative to dir: '../assets/images/'
 *
 * @param src the relative path of the image to
 */
function imgSrc(src: string) {
  return '../assets/images/' + src
}

export function getFooterTreeData() {
  return [
    createFooterData('Need help?', [
      'Contact us',
      'Resources',
      'Technical information',
      'Part number list',
      'Request quote',
      'Request samples',
      'Account application form',
      'Applications support/ Competitor cross reference'
    ]),
    createFooterData('Legal', [
      'Terms of Use',
      'Privacy',
      'IQD Ltd Standard Conditions of Sale',
      'IQD Inc Standard Conditions of Sale'

    ]),
    createFooterData('About IQD Frequency', [
      'About Us',
      'Careers',
      'Newsletter Signup',
      'News',
      'Blog'
    ])
  ]
}

export function createFooterData(desc: string, list: string[]) {
  return {desc, list, collapse: true}
}

/**
 * @implNote Content will seldom change, can be part of SPA.
 */
export function getPartnerContent() {
  // Helper lambda to save guard consistency (more eff than creating class)
  const create = (title: string, src: string, desc: string,) => {
    return {title, desc, link: src}
  }

  return [
    create('Technology Partner', imgSrc('partners/rakon.jpg'),
      'As a previous division of Rakon, IQD maintains a close partnership as a worldwide ' +
      'channel partner supplying Rakon’s specialist high performance oscillators alongside our own' +
      ' range. Rakon’s world leading timing solutions are particularly suitable for synchronising ' +
      'connectivity in telecom and network infrastructure, GPS positioning and satellite applications.'),
    create('Distribution Partner', imgSrc('partners/statek.jpg'),
      'IQD is a global authorised distributor for Statek Corporation’s highly specialised ' +
      'photolithographic process based quartz crystals, oscillators and sensors that offer very ' +
      'high environmental performance.')
  ]
}

/**
 * TODO - Dummy content which should be replaced with a call to DB, to allow for dynamic modification.
 */
export function getNewsItemsArr() {
  // Create class-object helper
  const create = (title: string, summary: string, date: string, src: string) => {
    return {title, summary, date, link: src}
  }

  return [
    create('Need extended Holdover from your Disciplined OCXO?',
      'The IQCM-200 is a disciplined OCXO, incorporating sync to a 1 PPS (pulse per second)' +
      ' input as well as a 1 PPS output. Its exceptional holdover stability and accuracy makes this particular' +
      ' product a superlative Disciplined OCXO...',
      '9th May 2022', imgSrc('need-extended-holdover-from-your-disciplined-ocxo.png')),
    create('Situation In Ukraine',
      'The situation in Ukraine is changing daily, new sanctions are initiated and the impact on the economy' +
      ' is only partly predictable. Therefore it is important for us to be in close contact with our customers...',
      '4th March 2022', imgSrc('ukrainian_flag.svg')),
    create('SMD Watch Crystal IQXC-217 Now Avaliable',
      '32.768kHz SMD Watch Crystals are still on allocation and lead times are in excess of 99 weeks.' +
      ' IQD have introduced the IQXC-217; a fit, form and function replacement for the CFPX-217...',
      '21st February 2022', imgSrc('iqxc-217.jpg')),
    create('Evaluation Board for Standard Oscillators',
      'How many times have you wished you had a quick and simple way of testing an oscillator? ' +
      'Now there is no need to worry about designing and building your own test circuit as you can use the' +
      ' new IOSC-EVBoard available from IQD and some of our distributors...',
      '1st November 2021', imgSrc('evaluation-board-standard-oscillators.jpg')),
    create('Miniature Clock Oscillator suits multiple applications',
      'Due to the severe shortage of parts in the standard package size of 3.2 x 2.5 mm,' +
      ' customers are moving more towards smaller packaged frequency products. Therefore, IQD wants ' +
      'to remind engineers of its 2 x 1.6 mm clock oscillator: IQXO-54x...',
      '4th October 2021', imgSrc('miniature-clock-oscillator.jpg')),
    create('Update on IQD’s high performance rubidium oscillator IQRB-2',
      'IQD is pleased to announce that the last piece of its extended rubidium portfolio has been ' +
      'updated and equipped with additional material: the IQRB-2. The IQRB-2 fills the gap between' +
      ' the IQRB-1 and the IQRB-3; the IQRB-1 delivers a...',
      '1st September 2021', imgSrc('update-on-rubidium-oscillator-iqrb-2.jpg')),
    create('IQD presents new low phase noise oscillators',
      'As application requirements drive the need for greater data rates, more oscillator devices' +
      ' with ultra-low phase noise are required. This type of application is especially prevalent in today\'s' +
      ' connected world. Applications include fibre transceivers, gigabit Ethernet and 5G base stations...',
      '7th June 2021', imgSrc('new-low-phase-noise-oscillators.jpg')),
    create('Low voltage clock oscillators',
      'Today, billions of electronic devices are developed to suit consumer needs. As technology' +
      ' evolves, devices need to run at lower and lower voltages and this means that systems may need to have' +
      ' multiple supply rails. Because of this IQD have produced a range of frequency products that...',
      '7th May 2021', imgSrc('low-voltage-clock-oscillators.jpg')),
  ]
}
