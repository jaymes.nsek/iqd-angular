import {Component, Input} from '@angular/core';
import {NewsItem} from "../interfaces";

/**
 * @implNote The list of {@link NewsItem} is bound via the  {@link newsArr} @Input;
 *           ------------------------------------------
 *           e.g. <app-news-latest-list [newsArr]="newsItems"> </app-news-latest-list>
 *
 * @implNote Insert class="show-img" or class="hide-img" to show or hide the card img, respectively.
 *          -------------------------------------------------------------
 *           E.g.: <app-news-latest-list class="show-img"> </app-news-latest-list>
 *
 * @implNote Mapped CSS Custom Properties, with examples and their default values:
 *
 *           Manipulation of the :host (that is, <app-news-latest-list></app-news-latest-list>)
 *                --display: block;
 *
 *                --grid-template-columns: 50% 50%;
 *
 *                // Overrides the second-to-last bottom border when showing multiple grid-columns;
 *                // so that the last two cards have no lines.
 *                // This is done by setting --second-to-last-btm-border to "none". default: solid
 *                --second-to-last-btm-border: solid;
 *
 *           <img>
 *                // The max-width allowed <img> width for each item's news-card; default: 100px
 *                --img-max-width: 100px;
 *
 *                // Removes (hides) the image from the DOM, thereby only the textual element of the
 *                // news-card is retained; or shows it, for which 'inline-block' is the recommended arg!
 *                // default: none
 *                --img-display: none;
 *
 *           div.news-card
 *                // ================================================================================
 *                // The total value set for  --column-gap-img-text, --grid-temp-col-width-img and
 *                // --grid-temp-col-width-txt should not exceed 100% of available width.
 *                // ================================================================================
 *
 *                // Set the in-news-card column-gap between the image and text container; default: 12px
 *                --column-gap-img-text: 12px;
 *
 *                // Set the first arg in 'grid-template-columns', allocated as the column width for the <img>,
 *                //  within the card; default: auto
 *                --grid-temp-col-width-img: auto;
 *
 *                // Set the second arg in 'grid-template-columns', allocated as the column width for the
 *                // text container (news description and date), within the card. default: 70%
 *                --grid-temp-col-width-txt: 70%;
 */
@Component({
  selector: 'app-news-latest-list',
  templateUrl: './news-latest-list.component.html',
  styleUrls: ['./news-latest-list.component.css']
})
export class NewsLatestListComponent {

  @Input() newsArr: NewsItem[] = []
}
