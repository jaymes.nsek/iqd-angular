import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsLatestListComponent } from './news-latest-list.component';

describe('LatestNewsComponent', () => {
  let component: NewsLatestListComponent;
  let fixture: ComponentFixture<NewsLatestListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewsLatestListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsLatestListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
