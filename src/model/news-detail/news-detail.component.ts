import {Component, Input, OnInit} from '@angular/core';
import {NewsItem} from "../interfaces";

/**
 * @implNote The displayed item is bound via the  {@link item} @Input;
 *           ------------------------------------------
 *           e.g. <app-news-detail [item]="topNews"> </app-news-detail>
 *
 * @implNote It is possible to include a single element, to act as a footer, using the component tags;
 *           ------------------------------------------
 *           e.g. <app-news-detail>
 *                    <h1>My Awesome Footer</h1>
 *                </app-news-detail>
 *
 * @implNote Set the minimum card image size via the component tag - px is the accepted unit.
 *           ------------------------------------------
 *           e.g. <app-news-detail>[minImgHeight]="200" </app-news-detail>
 *           NB: When used in tandem with grid or flex restrictions, the container size limit takes precedent,
 *           and hence the full value may not be honoured.
 *
 * @implNote Vertical and/or horizontal padding can be set via {@link verticalPad} and
 *           {@link horizontalPad}, respectively.
 */
@Component({
  selector: 'app-news-detail',
  templateUrl: './news-detail.component.html',
  styleUrls: ['./news-detail.component.css']
})
export class NewsDetailComponent implements OnInit {

  @Input() item: NewsItem | undefined

  /**
   * Left and Right padding, for the container, in px.
   */
  @Input() horizontalPad: number = 0

  /**
   * Top and Bottom padding, for the container, in px.
   */
  @Input() verticalPad: number = 0

  @Input() minImgHeight: number = 250

  ngOnInit(): void {
    // Callback on load, rather than DOMContentLoaded, is necessary so image content in news-latest-list
    // have assumed their position (been loaded) and as such an accurate height for the leftover
    // height in the parent container is established for imgWrapper in #initImgHeight.
    // Call helper #initImgHeight on resize, so that potentially image's new height is reflected on redraw
    const callback = () => {
      const element = document.getElementById('view-root')
      if (element != null)
        this.initImgHeight()
    }

    window.addEventListener('load', callback)
    window.addEventListener('resize', callback)
  }

  /**
   * Initialise the height of image to {@link #minImgHeight} or space initially adopted by the image's
   * wrapper, when used in a multi-columned grid with other components.
   *
   * @implNote Call after views (aka elements) have been instantiated in the DOM
   * @summary If the content of <img> is set via CSS, it knows no bounds, as such when used alongside
   *          {@link NewsLatestListComponent}, for example, the height of <img> can be grossly larger. This
   *          issue is solved by first allowing the image's wrapper <div> to wrap to available space,
   *          dictated by the partner Component, e.g., {@link NewsLatestListComponent}; It is this height or
   *          the minimum, set via {@link #minImgHeight} that is set as the height of <img>
   */
  private initImgHeight() {
    const imgWrapper = document.getElementById('img-wrap')
    const img = document.getElementById('card-img')

    if (img == null || imgWrapper == null)
      return;

    // Resetting to zero is of particular importance when the method is called to redraw
    //  onresize event - allows the available wrapper height to be recomputed.
    img.style.height = '0'

    // use the smallest length so that there is minimal disparity between this component and the other,
    // as explained in the @summary
    const width = imgWrapper.offsetWidth
    const height = imgWrapper.offsetHeight
    const len =  (height < width) ? height : width;

    // console.warn("imgWrapperHeight=" + len)

    img.style.height = ((len > this.minImgHeight) ? len : this.minImgHeight) + "px"
    img.style.display = 'inline-block'

    // console.warn("imgHeight=" + img.style.height)
  }
}
