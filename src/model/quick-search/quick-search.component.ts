import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-quick-search',
  templateUrl: './quick-search.component.html',
  styleUrls: ['./quick-search.component.css']
})
export class QuickSearchComponent {

  constructor() { }

  @Input() maxWidth: string = 'unset'

  // Provide class input options to eliminate erroneous class input via class="", on the component tag.
  @Input() columnType: 'one-column' | 'two-column' = 'two-column'
}
