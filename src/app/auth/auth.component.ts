import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {OnBrowserResizeListener} from "../../model/interfaces";
import {AppComponent} from "../app.component";
import {AuthService} from "../global/services/auth/auth.service";
import {ActivatedRoute, Event as NavEvent, NavigationStart, Router} from "@angular/router";
import {PATH} from "../global/constants/url-contants";
import {AuthType} from "../global/enums";
import {AuthPresenter} from "./auth.component.presenter";
import {filter, Subscription} from "rxjs";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {pattern} from "../global/helpers/custom-validator";

export interface AuthView {
  authType: AuthType
  activatedRoute: ActivatedRoute
}

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements AuthView, OnInit, OnBrowserResizeListener {
  @Output() showHeaderEvent = new EventEmitter<Boolean>()

  presenter: AuthPresenter
  AuthType = AuthType // Expose type to template

  // The set value determines whether certain UI are shown or hidden, in template.
  authType = AuthType.SIGN_IN

  height = 0

  navEvent$: Subscription|null = null

  /**
   * Password validation set via {@link #alignPasswordValidation}
   */
  formGroup: FormGroup = this.fb.group(
    {
      email: ['', [Validators.required, Validators.email]],
      password: ['']
    }
  )


  constructor(private authService: AuthService,
              public router: Router,
              public activatedRoute: ActivatedRoute,
              private fb: FormBuilder) {
    AppComponent.setOnBrowserResizeListener(this)

    this.presenter = new AuthPresenter(this)
  }

  ngOnInit(): void {
    // Signal that app-root should hide the header and #acc-benefit-banner section
    this.showHeaderEvent.emit(false)

    // Align UI to State
    this.authType = this.presenter.determineAuthState(this.presenter.getPath())
    // Align email validation rules to auth state
    this.alignPasswordValidation()

    this.registerNavStart()
  }

  onBrowserResize(menuHeight: number, contentHeight: number): void {
    this.height = contentHeight
  }

  //region Auth type toggles

  /**
   * @note The set value of {@link #authType} determines whether certain
   * groups of UI are shown, or hidden, in template.
   */
  onResetPwClicked() {
    this.authType = this.AuthType.RESET

    this.router.navigateByUrl(PATH.PASSWORD_RESET)
  }

  /**
   * @note The set value of {@link #authType} determines whether certain
   * groups of UI are shown, or hidden, in template.
   */
  onFooterNavClick() {
    // If currently in sign_in, update  to create_account, else link back to sign_in
    let path
    if (this.authType === AuthType.SIGN_IN) {
      this.authType = AuthType.CREATE_ACC
      path = PATH.REGISTER
    } else {
      this.authType = AuthType.SIGN_IN
      path = PATH.SIGN_IN
    }

    this.router.navigateByUrl(path)
  }

  //endregion

  /**
   * Returns one of 'SIGN IN' | 'REGISTER' | 'SEND', to indicate the current
   * auth-state on the singly-utilised button.
   */
  get buttonTxt() {
    return (this.authType === AuthType.SIGN_IN) ? 'SIGN IN' :
      (this.authType === AuthType.CREATE_ACC) ? 'REGISTER' : 'SEND'
  }

  onSubmit() {
    // Extract email and password from UI
    const credential = this.formGroup.getRawValue()
    const email = credential.email
    const password = credential.password

    if (this.authType === this.AuthType.SIGN_IN) {
      return this.signIn(email, password)
    } else if (this.authType === this.AuthType.CREATE_ACC) {
    }


    //console.warn('onSubmit: ', this.authType, 'redirectPath=', this.authService.redirectUrl)
  }

  signIn(email: string, password: string) {
    // TODO: Add Promise.catch to handle sign in errors
    this.authService.signIn(email, password).then(user => {
      console.warn('signIn: Checkpoint 0')
      // TODO - Add Paths ENUM where all valid paths/Redirects from contants.ts
      //  are collated, also set as type of authService.redirectUrl,
      //  so as to guard against potential url mistakes
      // User was redirected from another page, redirect back to it,
      // else redirect to homepage: '/account'
      const redirect = this.authService.redirectUrl ?? '/account'
      console.warn('signIn: ' + JSON.stringify(user), redirect)

      // sign-in successful
      if (user) {
        // unregister nav subscription before redirect so that the to avoid attempts to
        // determine state with non-auth url after redirect
        this.unregisterNavStart()
        this.router.navigateByUrl(redirect)
      }
    })
  }

  /**
   * Register NavigationStart listener, which detects navigations events triggered outside
   * the Router - for example, the browser's back/forward buttons
   */
  registerNavStart() {
    this.navEvent$ = this.router.events.pipe(
      filter((event: NavEvent) => {
        return (event instanceof NavigationStart)
      })
    ).subscribe((observer => {
      let navStart = <NavigationStart>observer

      // Align current path in url window to authType - so, template adopts correct UI state
      console.warn('registerNavStart: path = ', navStart.url)
      this.authType = this.presenter.determineAuthState(navStart.url)
      this.alignPasswordValidation()
    }))
  }

  unregisterNavStart() {
    if (this.navEvent$)
      this.navEvent$.unsubscribe()
  }

  /**
   * Set password validation inline with auth state;
   *
   * @summary remove validation when in {@link AuthType.RESET}, since the UI is not shown,
   * so the form can return as valid after a valid email is set.
   */
  alignPasswordValidation() {
    const pwFControl = this.formGroup.controls['password']

    if (this.authType === AuthType.SIGN_IN || this.authType === AuthType.CREATE_ACC)
      // Validators.pattern(this.presenter.RE_PASSWORD)
      pwFControl.setValidators(pattern(this.presenter.RE_PASSWORD))
    else
      pwFControl.clearValidators()

    pwFControl.updateValueAndValidity()
  }

  /**
   * Helper flags whether ValidationError is present; discounting non input as an error.
   *
   * @summary used as a cue to display error information in template.
   */
  hasValErr(attr: 'email' | 'password') {
    const fControl = this.formGroup.controls[attr]
    return fControl.invalid && fControl.value != ''
  }
}
