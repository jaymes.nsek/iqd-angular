import {AuthType} from "../global/enums";
import {PATH, SEG} from "../global/constants/url-contants";
import {AuthView} from "./auth.component";

export const ERR_UNHANDLED_AUTH_PATH = 'AuthComponentPresenter: the activated auth route is not presently implemented'

export class AuthPresenter {
  INVALID_EMAIL = "invalid email"
  INVALID_PW = "password requires at least 6 characters, containing uppercase, lowercase, number and symbol"

  RE_PASSWORD!: RegExp

  constructor(private view: AuthView) {
    this.initPasswordRE()
  }

  private initPasswordRE() {
    // Valid password character grouping
    const PW_SYM = ',;:_*"^¨~<>\'!#%/&=?+|`()@.-'
    const PW_RE = `[A-Za-z0-9${PW_SYM}]`

    // Password is at least 6 chars, contains at least one of each: Uppercase, Lowercase, num and sym
    this.RE_PASSWORD = new RegExp(
      `^(?=${PW_RE}*[A-Z])(?=${PW_RE}*[a-z])(?=${PW_RE}*[0-9])(?=${PW_RE}*[${PW_SYM}])(${PW_RE}{6,})$`,
      'g'
    )
  }

  /**
   * @param path is the relative auth url to the domain, e.g. "/auth/sign-in" --- OR ---
   * its end segment, e.g. "sign-in"
   */
  determineAuthState(path: string): AuthType {
    switch (path) {
      case SEG.REGISTER:
      case PATH.REGISTER:
        return AuthType.CREATE_ACC
      case SEG.SIGN_IN:
      case PATH.SIGN_IN:
        return AuthType.SIGN_IN
      case SEG.PASSWORD_RESET:
      case PATH.PASSWORD_RESET:
        return AuthType.RESET
      default:
        throw new Error(ERR_UNHANDLED_AUTH_PATH)
    }
  }

  getPath() {
    return this.view.activatedRoute.snapshot.url[0].path
  }
}
