import {AuthPresenter, ERR_UNHANDLED_AUTH_PATH} from "./auth.component.presenter";
import {ActivatedRoute} from "@angular/router";
import {AuthType} from "../global/enums";
import {AuthView} from "./auth.component";


describe('AuthPresenter', () => {
  let presenter: AuthPresenter
  let spyActivatedRoute: ActivatedRoute
  let spyAuthView: AuthView

  beforeEach(() => {
    spyActivatedRoute = new ActivatedRoute()
    spyAuthView = {
      authType: AuthType.SIGN_IN,
      activatedRoute: spyActivatedRoute
    }

    presenter = new AuthPresenter(spyAuthView)
  })

  describe('AuthPresenter (POJsO)', () => {
    describe('#REG_EX_PASSWORD', () => {
      // at least 6 characters, containing uppercase, lowercase, number and symbol

      it('should PASS with valid char set - 6x, UC, lc, num and sym ',  () => {
        expect(presenter.RE_PASSWORD.test("ABc-5#")).toBeTrue()
      })

      const params = [
        ['len > 6', 'ABc5#'],
        ['Uppercase', 'queen19#'],
        ['Lowercase', 'KING19#'],
        ['Number', 'Queen-s#'],
        ['Symbol', 'QueenS62'],
      ]

      params.forEach(([desc, password]) => {
        it(`should FAIL even if only faulting "${desc}"`,  () => {
          expect(presenter.RE_PASSWORD.test(password)).toBeFalse()
        })
      })
    })
  })

  describe('#determineAuthState', () => {
    it('should throw Error for an invalid path', () => {
      expect(function () {presenter.determineAuthState('unsupported-path')})
        .toThrowError(Error, ERR_UNHANDLED_AUTH_PATH)
    })

    describe('valid url', () => {
      const params = [
        ['should return AuthType.CREATE_ACC', 'register', AuthType.CREATE_ACC],
        ['should return AuthType.CREATE_ACC', '/auth/register', AuthType.CREATE_ACC],
        ['should return AuthType.SIGN_IN', 'sign-in', AuthType.SIGN_IN],
        ['should return AuthType.SIGN_IN', '/auth/sign-in', AuthType.SIGN_IN],
        ['should return AuthType.RESET', 'reset', AuthType.RESET],
        ['should return AuthType.RESET', '/auth/reset', AuthType.RESET]
      ]

      params.forEach(([desc, path, expected]) => {
        it( `${desc} ["${path}"]`, () => {
          expect(presenter.determineAuthState(path)).toBe(expected)
        })
      })
    })

  })
})
