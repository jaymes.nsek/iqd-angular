import {ComponentFixture, TestBed} from '@angular/core/testing'
import {AuthComponent} from './auth.component'
import {RouterTestingModule} from "@angular/router/testing"
import {AuthType} from "../global/enums";
import {ActivatedRoute, Router} from "@angular/router";
import {MockActivatedRoute, MockRouter} from "../global/fakes/mocks";
import {FormBuilder, FormsModule, ReactiveFormsModule} from "@angular/forms";

// Selectors
const resetBtnSelector = 'button[id="reset-pw-btn"]'
const footerNavBtnSelector = 'button[class="footer-nav-btn"]'
const footerNavClickSelector = 'button[class="footer-nav-btn"] a[class="footer-nav"]'

// Common it() params, across states, for DRY
const commonParams = {
  renderEmailLabel: [
    'should render email label as "E-mail"', 'div[id="email-wrap"] > label[for="email"]', 'E-mail'
  ],
  renderEmailInput: [
    'should render empty-string for email text input', 'div[id="email-wrap"] > input[id="email"]', ''
  ],
  renderPwLabel: [
    'should render password label as "Password"', 'div[id="password-wrap"] > label[for="password"]', 'Password'
  ],
  renderPwInput: [
    'should render empty string for password text input', 'div[id="password-wrap"] > input[id="password"]', ''
  ]
}


describe('AuthComponent', () => {
  let component: AuthComponent
  let fixture: ComponentFixture<AuthComponent>
  let rootElement: HTMLElement

  function applyFixtureChanges() {
    fixture.detectChanges()
    rootElement = fixture.nativeElement as HTMLElement
  }

  /**
   * @param state the mock {@link AuthType} to return for
   * {@link AuthPresenter#determineAuthState} when called in {@link AuthComponent}
   */
  function configureTestBed(state: AuthType) {
    beforeEach(async () => {
      await TestBed.configureTestingModule({
        declarations: [
          AuthComponent
        ],
        imports: [
          RouterTestingModule,
          FormsModule,
          ReactiveFormsModule
        ],
        providers: [
          {provide: Router, useClass: MockRouter},
          // MockActivatedRoute causes this.activatedRoute.snapshot.url[0].path to always return
          //  empty - "" - , but this does not affect test as #determineAuthState is mocked too
          {provide: ActivatedRoute, useClass: MockActivatedRoute},
          {provide: FormBuilder}
        ]
      }).compileComponents()
    })

    beforeEach(() => {
      fixture = TestBed.createComponent(AuthComponent)
      component = fixture.componentInstance

      //Install the spy when created but before component.ngOnInit() is invoked
      spyOn(component.presenter, 'determineAuthState')
        .and.returnValue(state)

      spyOn(component, 'registerNavStart').and.returnValue()

      applyFixtureChanges()
    })
  }

  /**
   * @note The drawback of applying DRY in this test suite is lack of compile-time validation
   * of the selector strings, however, the benefit of DRY outweighs it.
   * Also, to overcome this, selector strings may, first, be tested on rootElement#querySelector
   */
  describe('Auth States', () => {
    describe('"sign-in" state [initial state]', () => {
      configureTestBed(AuthType.SIGN_IN)

      it('should have authType as SIGN_IN', () => {
        expect(component).not.toBeUndefined()

        // the initial state is SIGN_IN
        expect(component.authType).toBe(AuthType.SIGN_IN)
      })

      const params = [
        ['should render auth title as "Sign In"', 'h1[id="header-title"]', 'Sign In'],
        commonParams.renderEmailLabel,
        commonParams.renderEmailInput,
        commonParams.renderPwLabel,
        commonParams.renderPwInput,
        ['should render submit button text as "SIGN IN"', 'button[id="submit-btn"]', 'SIGN IN'],
        ['should render #reset-pw-btn as "Forgotten your password?"', resetBtnSelector,
          'Forgotten your password?'],
        ['should render #footer text as "Create an account with IQD Frequency Products"',
          footerNavBtnSelector, 'Create an account with IQD Frequency Products']
      ]

      params.forEach(([desc, selector, expected]) => {
        it(desc, () => {
          const element = <HTMLElement>rootElement.querySelector(selector)

          expect(element).not.toBeNull()
          expect(element?.textContent).toBe(expected)
          expect(getComputedStyle(element).display).not.toEqual('none')
        })
      })
    })

    describe('"register" state [transitioned from "sign-in"]', () => {
      configureTestBed(AuthType.SIGN_IN)

      /**
       * simulate click via element.click() to also cover the HTML binding callback.
       */
      function clickFooter() {
        const footerClick = <HTMLElement>rootElement.querySelector(footerNavClickSelector)

        footerClick.click()
        applyFixtureChanges()
      }

      it('should initially have authType as SIGN_IN', () => {
        expect(component).not.toBeUndefined()

        // the initial state is SIGN_IN
        expect(component.authType).toBe(AuthType.SIGN_IN)
      })

      describe('Shown Elements', () => {
        beforeEach(() => {
          clickFooter()
        })

        const params = [
          ['should render auth title as "Create Account"', 'h1[id="header-title"]', 'Create Account'],
          commonParams.renderEmailLabel,
          commonParams.renderEmailInput,
          commonParams.renderPwLabel,
          commonParams.renderPwInput,
          ['should render submit button text as "REGISTER"', 'button[id="submit-btn"]', 'REGISTER'],
          ['should render #footer text as "Already have an account? Sign in"', footerNavBtnSelector,
            'Already have an account? Sign in']
        ]

        params.forEach(([desc, selector, expected]) => {
          it(desc, () => {
            expect(component.authType).toBe(AuthType.CREATE_ACC)

            const element = <HTMLElement>rootElement.querySelector(selector)

            expect(element).not.toBeNull()
            expect(element?.textContent).toBe(expected)
            expect(getComputedStyle(element).display).toEqual('block')
          })
        })
      })

      describe('Hidden Elements', () => {
        it('should hide #reset-pw-btn (situated underneath submit btn)', () => {
          clickFooter()
          expect(component.authType).toBe(AuthType.CREATE_ACC)

          const element = <HTMLElement>rootElement.querySelector(resetBtnSelector)

          expect(element).not.toBeNull()
          expect(getComputedStyle(element).display).toEqual('none')
        })
      })
    })

    describe('"reset" state [transitioned from "sign-in"]', () => {
      configureTestBed(AuthType.SIGN_IN)

      /**
       * simulate click via element.click() to also cover the HTML binding callback.
       */
      function clickResetPW() {
        const resetPW = <HTMLElement>rootElement.querySelector(resetBtnSelector)

        resetPW.click()
        applyFixtureChanges()
      }

      it('should initially have authType as SIGN_IN', () => {
        expect(component).not.toBeUndefined()

        // the initial state is SIGN_IN
        expect(component.authType).toBe(AuthType.SIGN_IN)
      })

      describe('Shown Elements', () => {
        const params = [
          ['should render auth title as "Reset Password"', 'h1[id="header-title"]', 'Reset Password'],
          commonParams.renderEmailLabel,
          commonParams.renderEmailInput,
          ['should render submit button text as "SEND"', 'button[id="submit-btn"]', 'SEND'],
          ['should render #footer text as "Return back to Sign in"', footerNavBtnSelector,
            'Return back to Sign in']
        ]

        params.forEach(([desc, selector, expected]) => {
          it(desc, () => {
            clickResetPW()
            expect(component.authType).toBe(AuthType.RESET)

            const element = <HTMLElement>rootElement.querySelector(selector)
            expect(element).not.toBeNull()
            expect(element?.textContent).toBe(expected)
            expect(getComputedStyle(element).display).toEqual('block')
          })
        })
      })

      describe('Hidden Elements', () => {
        const params = [
          ['should hide password container, with label and input', 'div[id="password-wrap"]'],
          ['should hide #reset-pw-btn (situated underneath submit btn)', resetBtnSelector]
        ]

        params.forEach(([desc, selector]) => {
          it(desc, () => {
            clickResetPW()
            expect(component.authType).toBe(AuthType.RESET)

            const element = <HTMLElement>rootElement.querySelector(selector)
            expect(element).not.toBeNull()
            // A hidden password container means nested label and input are hidden
            expect(getComputedStyle(element).display).toEqual('none')
          })
        })
      })
    })
  })
})
