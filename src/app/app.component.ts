import {Component, OnDestroy, OnInit} from '@angular/core';
import {DomSanitizer, Title} from "@angular/platform-browser";
import {OnBrowserResizeListener} from "../model/interfaces";
import {getFooterTreeData} from "../model/helper";
import {MatIconRegistry} from "@angular/material/icon";
import {getStyleAfterDOMLoad} from "../model/shared";
import {firstSegment, PATH} from "./global/constants/url-contants";
import {CHEVRON_DOWN_ICON, CHEVRON_UP_ICON, HOUSE_ICON, PHONE_ICON} from "./app.component.constants"
import {AuthService} from "./global/services/auth/auth.service";
import {Event as NavEvent, NavigationEnd, Router} from "@angular/router";
import {filter, Subscription} from "rxjs";
import {UI_TEXT} from "./global/constants/ui-constants";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'IQD-Angular'

  // Expose imports to html template
  PATH = PATH
  UI_TEXT = UI_TEXT

  showHeader = true

  //Browser's height = contentHeight + menuHeight.
  private static contentHeight: number = 0;
  private static menuHeight: number = 0; // represents the height of the App Bar (aka Header)

  private static browserResizeLis: OnBrowserResizeListener | null = null;

  treeList: { desc: string, list: string[], collapse: boolean }[] // Footer's tree info
  isCollapsingFooter = true // Tracked value from CSS, indicates if footer's tree-list is collapse-able - by toggling

  // TODO: For now it isn't certain whether menuItems and headerItems will converge,
  //  if so, they can be merged as one!
  menuItems = [
    {name: 'Home', path: '/', isSelected: false},
    {name: 'Products', path: PATH.PRODUCTS, isSelected: false},
    //{name: 'Contact Us', path: '/account'},
  ]

  headerItems = [
    {name: 'Home', path: '/', isSelected: false},
    {name: 'Products', path: PATH.PRODUCTS, isSelected: false},
    //{name: 'Contact Us', path: '/account'},
  ]

  private navEndEvent$: Subscription | null = null

  constructor(private titleService: Title,
              private authService: AuthService,
              private router: Router,
              iconRegistry: MatIconRegistry,
              sanitizer: DomSanitizer) {
    this.titleService.setTitle(this.title)
    this.treeList = getFooterTreeData()

    // Register icons
    iconRegistry.addSvgIconLiteral('chevron-up', sanitizer.bypassSecurityTrustHtml(CHEVRON_UP_ICON))
    iconRegistry.addSvgIconLiteral('chevron-down', sanitizer.bypassSecurityTrustHtml(CHEVRON_DOWN_ICON)) // collapse state
    iconRegistry.addSvgIconLiteral('phone', sanitizer.bypassSecurityTrustHtml(PHONE_ICON))
    iconRegistry.addSvgIconLiteral('house', sanitizer.bypassSecurityTrustHtml(HOUSE_ICON))
  }

  ngOnInit(): void {
    // -  Set initial value (it seems, calculated without margins)
    this.setHeights()

    addEventListener('DOMContentLoaded', () => {
      // Update initial values and notify listener
      this.setHeights()
      AppComponent.notifyOnBrowserResizeListeners();
    })

    const flagIsFooterCollapse = () => {
      getStyleAfterDOMLoad('id_root_area', '--isCollapsingFooter')
        .then((flag => {
          this.isCollapsingFooter = (flag === "true")
        }))
    }

    flagIsFooterCollapse() // Call to flag prop when Component is first init

    window.onresize = () => {
      // Recompute mandatory/optimal dimensions
      // Re-acquire menuHeight, as it is likely to change with different view width
      this.setHeights();
      AppComponent.notifyOnBrowserResizeListeners();

      // Flag collapsing footer if window is resized
      flagIsFooterCollapse()
    };

    this.registerNavFlaggerLis()
  }

  ngOnDestroy(): void {
    window.onresize = null
    AppComponent.browserResizeLis = null;

    this.unregisterNavFlaggerLis()
  }

  setHeights(): void {
    AppComponent.menuHeight = AppComponent.calcMenuHeight();
    // subtract toolbar/menu height (so that callers can properly center content).
    AppComponent.contentHeight = window.innerHeight - AppComponent.menuHeight;
  }

  /**
   * Retrieve the Toolbar height.
   */
  private static calcMenuHeight(): number {
    const element = window.document.getElementById("appHeader");

    return element == null ? 0 : element.offsetHeight;
  }

  /**
   * Provided to expose {@link AppComponent#menuHeight} for binding; but also to avoid the need
   * to repeatedly query resource intense {@link AppComponent#calcMenuHeight} during each UI drawing.
   */
  getMenuHeight(): number {
    return AppComponent.menuHeight
  }

  get contentHeight(): number {
    return AppComponent.contentHeight
  }

  static setOnBrowserResizeListener(l: OnBrowserResizeListener | null) {
    this.browserResizeLis = l;
    this.notifyOnBrowserResizeListeners();
  }

  private static notifyOnBrowserResizeListeners() {
    if (this.browserResizeLis) {
      this.browserResizeLis.onBrowserResize(this.menuHeight, this.contentHeight);

      // Snap the body to top after the page is loaded so that the intro section is shown.
      document.body.scrollTop = 0; // For Safari
      document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
    }
  }

  /**
   *
   * @param index the index of the clicked header in {@link #treeList}
   */
  handleTreeHeader(index: number) {
    // Toggle the corresponding collapse state
    this.treeList[index].collapse = !this.treeList[index].collapse
  }

  //region router-outlet events

  async onActivate(event: any) {
    const authCompEvent = event.showHeaderEvent
    // Hide the header when Auth is routed (signalled by authCompEvent)
    this.showHeader = !authCompEvent

    // Unset margin, padding and max-width so AuthComponent can expand to fullscreen
    let main
    if (authCompEvent && (main = document.querySelector('main'))) {
      main.style.margin = 'unset'
      main.style.maxWidth = 'unset'
      main.style.padding = 'unset'
    }
  }

  onDeactivate(event: any) {
    const authCompEvent = event.hideHeaderEvent
    if (authCompEvent)
      authCompEvent.unsubscribe()
  }

  //endregion

  //region router-link helpers

  /**
   * Return true if user present, i.e. logged in, else false.
   */
  get hasLoggedIn() {
    return !!this.authService.user
  }

  /**
   * Sign out user and return to Homepage: domain.com/
   */
  signOutAndRedirect() {
    this.authService.signOut()
    this.router.navigateByUrl('/')
  }

  redirectToSignIn() {
    this.router.navigateByUrl(PATH.SIGN_IN)
  }

  //endregion

  //region Header Nav Listener - For flagging current page

  /**
   * Register NavigationEnd listener for header, using the event to highlight a header
   * item corresponding to the navigated URL.
   *
   * @summary Header items are activated (or deactivated) in NavEnd event so that in the event
   * that a clicked UI is not mapped in the list of header items, it is not erroneously flagged as
   * the previous item on the header.
   * @summary it also detects navigations events triggered outside
   * the Router - for example, the browser's back/forward buttons.
   */
  registerNavFlaggerLis() {
    this.navEndEvent$ = this.router.events.pipe(
      filter((event: NavEvent) => {
        return (event instanceof NavigationEnd)
      })
    ).subscribe((observer => {
      let navEnd = <NavigationEnd>observer

      // If url seg matches (including the root seg), highlight header, else enforce clear
      const rootPath = '/' + firstSegment(navEnd.url)
      for (let item of this.headerItems)
        item.isSelected = (item.path === navEnd.url) || item.path === rootPath

      for (let item of this.menuItems)
        item.isSelected = item.path === navEnd.url || item.path === rootPath
    }))
  }

  unregisterNavFlaggerLis() {
    if (this.navEndEvent$)
      this.navEndEvent$.unsubscribe()
  }

  //endregion
}
