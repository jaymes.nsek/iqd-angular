import {NgModule} from '@angular/core';
import {RouterModule, Routes, UrlMatchResult, UrlSegment} from '@angular/router';
import {HomeComponent} from "./home/home.component";
import {AuthComponent} from "./auth/auth.component";
import {MyAccountComponent} from "./my-account/my-account.component";
import {AuthGuard} from "./global/guards/auth.guard";
import {PATH} from "./global/constants/url-contants";
import {ProductComponent} from "./product/product.component";
import {ProductTypeComponent} from "./product/product-type/product-type.component";

/**
 * Route matcher function for the end paths of
 * {@link PATH#SIGN_IN}, {@link PATH#REGISTER} and {@link PATH#PASSWORD_RESET},
 * and corresponding to the children of /auth/.
 */
export function authPathChildMatcher(url: UrlSegment[]): UrlMatchResult | null {
  const regEx = new RegExp(`^sign-in$|^register$|^reset$`)
  return url.length === 1 && url[0].path.match(regEx) ?
    {consumed: url} : null
}

export function productPathChildMatcher(url: UrlSegment[]): UrlMatchResult | null {
  const regEx = new RegExp(`^quartz-crystal$|^clock-oscillators$`)
  return url.length === 1 && url[0].path.match(regEx) ?
    {consumed: url} : null
}

const routes: Routes = [
  {
    path: "", component: HomeComponent
  }, // {path: "", redirectTo: "/home", pathMatch: "full"}
  {
    path: 'auth',
    children: [
      {matcher: authPathChildMatcher, component: AuthComponent}
    ]
  },
  {
    path: 'products', component: ProductComponent
  },
  {
    path: 'products',
    children: [
      {matcher: productPathChildMatcher, component: ProductTypeComponent}
    ]
  },
  /*{path: "auth/:authType", component: AuthComponent},*/
  {
    path: 'account',
    component: MyAccountComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "home", component: HomeComponent
  },
  {
    path: "product", component: ProductComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
