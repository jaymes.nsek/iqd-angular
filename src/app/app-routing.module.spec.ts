import {authPathChildMatcher} from "./app-routing.module";
import {UrlMatchResult, UrlSegment} from "@angular/router";

describe('AppRoutingModule', () => {

  describe('#authPathChildMatcher', () => {

    describe('a valid auth path',  () => {
      const params = [
        'sign-in',
        'register',
        'reset'
      ]

      params.forEach(path => {
        it(`should return UrlMatchResult [${path}]`,  () => {
          const seg: UrlSegment = new UrlSegment(path, {})
          const actual: UrlMatchResult | null = authPathChildMatcher([seg])

          expect(actual).not.toBeNull()
          expect(actual?.consumed).toHaveSize(1)
          expect(actual?.consumed[0].path).toEqual(path)
        })
      })
    })

    describe('UrlSegment#path does not exactly match valid auth paths',  () => {

      const params = [
        'not-supported',
        'xregister', // "x" depicts a condition where there is a match, but NOT exact
        'resetx',
        '/register' // Should not have "/" when being examined
      ]

      params.forEach(path => {
        it(`should return null [${path}]`,  () => {
          const seg: UrlSegment = new UrlSegment(path, {})
          const actual: UrlMatchResult | null = authPathChildMatcher([seg])

          expect(actual).toBeNull()
        })
      })
    })
  })

})
