import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HomeComponent} from './home/home.component';
import {NoopAnimationsModule} from "@angular/platform-browser/animations";
import {TabComponent} from "../model/Tab/tab.component";
import {QuickSearchComponent} from '../model/quick-search/quick-search.component';
import {ReactiveFormsModule} from "@angular/forms";
import {NewsLatestListComponent} from '../model/news-latest-list/news-latest-list.component';
import {NewsDetailComponent} from '../model/news-detail/news-detail.component';
import {AuthComponent} from './auth/auth.component';
import {MyAccountComponent} from './my-account/my-account.component';
import {AuthGuard} from "./global/guards/auth.guard";
import {AppMatModule} from "./app-mat.module";
import { ProductComponent } from './product/product.component';
import { ProductTypeComponent } from './product/product-type/product-type.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TabComponent,
    QuickSearchComponent,
    NewsLatestListComponent,
    NewsDetailComponent,
    AuthComponent,
    MyAccountComponent,
    ProductComponent,
    ProductTypeComponent
  ],
  imports: [
    AppMatModule,
    BrowserModule,
    NoopAnimationsModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule {
}
