import {Component, OnInit} from '@angular/core';
import {getNewsItemsArr, getPartnerContent, getTabContent} from "../../model/helper";
import {LinkItem, NewsItem, PartnerItem, TabItem} from "../../model/interfaces";
import {getStyleAfterDOMLoad} from "../../model/shared";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  tabArr: TabItem[]
  partnerArr: PartnerItem[]

  newsItemsOriginal: NewsItem[] | null = null
  newsItems: NewsItem[] = []
  isNewsViewVisible = false
  is1columnQSearch = false // quick-search column type

  // Array of GetSupport objects
  getSupportArr: LinkItem[] = [
    {title: 'Applications Support/Competitor Cross Reference', link: ''},
    {title: 'Request Quotation/Samples', link: ''},
    {title: 'Credit Application Form', link: ''},
    {title: 'ISO9001 Certificate', link: ''},
    {title: 'RoHS/REACh & Conflict Mineral Policies', link: ''},
  ]

  constructor() {

    // TODO Pass external content via <component> props, in AppComp
    this.tabArr = getTabContent()
    this.partnerArr = getPartnerContent()
  }

  ngOnInit(): void {
    const callback = () => {
      //region Handle synchronisation between news-detail and news-latest-list, to avoid duplicating the top news
      // Flag visibility of news-detail
      const newsView = document.getElementById('newsView')
      this.isNewsViewVisible = (newsView?.offsetHeight != 0)

      // Must be called here, after this.isNewsViewVisible has initialised!
      // @ >1200px, add two more rows to news-latest-list, this should be tied with CSS code when
      // 2 grid-column is activated.
      const windowWidth = window.innerWidth
      this.newsItems = this.getNewsItems((windowWidth > 1200) ? 7 : 5)
      //endregion

      // init quick-search-component column-type
      getStyleAfterDOMLoad('home-quick-search', '--column-type')
        .then(value => {
          this.is1columnQSearch = (value === 'one-column')
        })
    }

    // DOMContentLoaded handles the first time the window is loaded, whilst onresize
    // handles the case where the user resizes the window after a load event has occurred.
    window.addEventListener('DOMContentLoaded', callback)
    window.addEventListener('resize', callback)
  }

  /**
   * Return the top news in the news array, represented as element 0; Lazy init.
   */
  get topNews() {
    if (this.newsItemsOriginal == null)
      this.newsItemsOriginal = getNewsItemsArr()

    return this.newsItemsOriginal[0]
  }

  /**
   * Return the array to display in news-latest-list;
   *
   * @param count the max number of news items to include; default is 5.
   * @summary returns the original array if {@link isNewsViewVisible} is true,
   *          else an array, without element 0, is returned.
   */
  getNewsItems(count: number = 5) {
    if (this.newsItemsOriginal == null)
      this.newsItemsOriginal = getNewsItemsArr()

    // Remove element0 from the array passed over to news-latest-list (as it is being shown in news-detail)
    return this.newsItemsOriginal.slice((this.isNewsViewVisible) ? 1 : 0, count)
  }
}
