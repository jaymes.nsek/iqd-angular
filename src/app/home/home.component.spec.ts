import {ComponentFixture, TestBed} from '@angular/core/testing'
import {HomeComponent} from './home.component'
import {TabComponent} from "../../model/Tab/tab.component"
import {QuickSearchComponent} from "../../model/quick-search/quick-search.component"
import {FormsModule} from "@angular/forms";
import {NewsDetailComponent} from "../../model/news-detail/news-detail.component";
import {NewsLatestListComponent} from "../../model/news-latest-list/news-latest-list.component";
import {AppMatModule} from "../app-mat.module";


describe('HomeComponent', () => {
  let component: HomeComponent
  let fixture: ComponentFixture<HomeComponent>

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        HomeComponent, TabComponent, QuickSearchComponent,
        NewsDetailComponent, NewsLatestListComponent
      ],
      imports:[AppMatModule, FormsModule]
    }).compileComponents()
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
