import {Component, OnDestroy, OnInit} from '@angular/core';
import {ProductService} from "../../global/services/product/product.service";
import {ActivatedRoute} from "@angular/router";
import {ProductType} from "../../global/models";
import {getARUrlSegment} from "../../global/constants/url-contants";
import {AppComponent} from "../../app.component";
import {OnBrowserResizeListener} from "../../../model/interfaces";

@Component({
  selector: 'app-product-type',
  templateUrl: './product-type.component.html',
  styleUrls: ['./product-type.component.css']
})
export class ProductTypeComponent implements OnInit, OnBrowserResizeListener, OnDestroy {
  productType: ProductType | undefined = undefined

  isExpand = false
  maxDescHeight = 0

  constructor(private activatedRoute: ActivatedRoute, public productService: ProductService) {
    this.getProductType()
  }

  ngOnInit(): void {
    AppComponent.setOnBrowserResizeListener(this)
  }

  ngOnDestroy(): void {
    AppComponent.setOnBrowserResizeListener(null)
  }

  onBrowserResize(menuHeight: number, contentHeight: number): void {
    this.maxDescHeight = contentHeight
  }

  private getProductType() {
    // Use path (activated segment) to determine productType title, example
    const segment = getARUrlSegment(this.activatedRoute)
    this.productService.getProductType(segment).then(productType => this.productType = productType)
  }

  onDescExpanderClick() {
    this.isExpand = !this.isExpand
  }
}
