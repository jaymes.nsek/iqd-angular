import {Component, OnInit} from '@angular/core';
import {ProductType} from "../global/models";
import {ProductService} from "../global/services/product/product.service";

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  productTypeList: ProductType[] = []

  constructor(private productService: ProductService) {
    productService.getProductTypes().then(list => {
      this.productTypeList = list
    })
  }


  ngOnInit(): void {
  }
}
