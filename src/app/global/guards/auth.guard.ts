import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import { Observable } from 'rxjs';
import {AuthService} from "../services/auth/auth.service";
import {PATH} from "../constants/url-contants";

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private authService: AuthService, private router: Router) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const user = this.authService.user

    if (user)
      return true

    // User not logged-in: redirect to /auth/log-in! First, flag the url-of-interest so,
    // after a successful auth, it maybe redirected to
    console.warn('this.authService.redirectUrl=', state.url)

    this.authService.redirectUrl = state.url
    console.warn('canActivate: redirectUrl=', this.authService.redirectUrl)
    return this.router.parseUrl(PATH.SIGN_IN)
  }
}
