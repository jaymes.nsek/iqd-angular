/**
 * User's JWT token
 */
export interface User {
  email: string
}

export interface ProductType { name: string, desc: string, imgUrl: string, path: string }
