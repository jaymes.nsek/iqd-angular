import {AbstractControl, ValidationErrors, ValidatorFn} from "@angular/forms";

export function pattern(re: RegExp): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const flag = re.test(control.value)

    return flag ? null : {customControl: {value: control.value}}
  }
}
