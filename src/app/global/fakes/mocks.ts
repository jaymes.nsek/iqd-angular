import {NavigationBehaviorOptions, NavigationExtras, UrlTree} from "@angular/router";

/**
 * Use the mock router to isolate the test to AuthComponent
 */
export class MockRouter {
  navigateByUrl(url: string | UrlTree, extras?: NavigationBehaviorOptions): Promise<boolean> {
    return Promise.resolve(true)
  }
}

export class MockActivatedRoute {
  snapshot = {
    url: [
      {path: ''} // The path part of a URL segment, blank as current fulfilled via spy
    ]
  }
}
