
export  enum AuthType {
  SIGN_IN = 'Sign In',
  CREATE_ACC = 'Create Account',
  RESET = 'Reset Password'
}
