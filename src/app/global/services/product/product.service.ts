import {Injectable} from '@angular/core';
import {lastSegment, PATH} from "../../constants/url-contants";
import {ProductType} from "../../models";

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  /**
   *
   * @summary {name, imgUrl, desc} are from backend, and path attribute is derived in ProductService
   */
  private productTypeList: ProductType[] = [
    {
      name: 'Quartz Crystals',
      imgUrl: './assets/backend-assets/prod-catgeory/quartz-crystals.png',
      desc: 'Quartz crystal resonators are used in a wide range of electronic applications that require' +
        ' timing devices. The quality of the quartz crystal blank is critical to the performance of quartz ' +
        'crystal units and the high Q factor of quartz means that tight stabilities can be achieved; this is' +
        ' key to ensuring microprocessor timing circuits work correctly within customer’s designs. IQD has been' +
        ' a quartz crystal (XTAL) manufacturer for over 40 years and offers a comprehensive range of quartz ' +
        'crystal resonators covering both surface mount (SMD) and legacy thru-hole quartz crystal units that ' +
        'satisfy virtually all applications.\n\n' +
        'Our range includes various types of crystal (xtal) units from simple watch crystals (xtals) to high ' +
        'performance crystal resonators for industrial and medical applications including high shock and vibration' +
        ' versions that operate over extended operating temperature ranges up to -55 to 200°C. We offer a wide ' +
        'choice of package sizes down to 1.55 x 0.95mm, frequency tolerance and temperature stability down to ' +
        '5ppm, frequencies from 10kHz to 250MHz and models that satisfy specialist requirements such as low aging' +
        ' and low phase noise. In addition, our range of automotive crystals to AEC-Q200 and TS16949 are available' +
        ' with PPAP levels 1 to 5.\n\n' +
        'IQD is also a global authorised distributor for Statek Corporation’s highly specialised photolithographic' +
        ' process based quartz crystals, oscillators and sensors that offer very high environmental performance.',
      path: PATH.QUARTZ_CRY
    },
    {
      name: 'Clock Oscillators',
      desc: 'A crystal clock oscillator (sometimes referred to as a \'Standard Packaged Crystal Oscillator\' ' +
        'or SPXO) is a \'plug and play\' quartz crystal. It combines a quartz crystal resonator with an oscillator ' +
        'circuit in a single package to provide a fully functioning stand-alone oscillator circuit. They are used' +
        ' in many telecom, industrial and consumer applications. IQD’s comprehensive range of crystal clock ' +
        'oscillators are available in a range of voltages down to 1.8V and in a variety of package sizes including ' +
        'both miniature surface mount (SMD) and thru-hole packages. Our low power oscillators are particularly ' +
        'suitable for use in handheld battery operated products. We also offer low jitter and low phase noise ' +
        'models and many alternative outputs including CMOS, HCMOS, ACMOS, LVPECL, LVDS and TTL. In addition, our ' +
        'range of factory programmable oscillators allow for very short lead times down to 3 days for custom ' +
        'frequencies and are available in a variety of packages sizes and specifications.\n\n' +
        'IQD\'s range of automotive clock oscillators are suitable for most automotive applications and are available ' +
        'to AEC-Q200 and TS16949 are available with PPAP levels 1 to 5. Typical applications for IQD\'s automotive ' +
        'oscillators include ABS and Airbag sensors, Engine Control Units (ECU), High speed automotive networks and ' +
        'Tyre Pressure Monitoring System (TPMS) receivers.\n\n' +
        'As a previous division of Rakon, IQD maintains a close partnership as a worldwide channel partner supplying ' +
        'Rakon’s wide range of high performance and high reliability Crystal Oscillators(XOs). The high performance ' +
        'range of XOs feature low jitter, low phase noise, and selectable output frequency options; these features ' +
        'make them ideal for applications in Global Positioning and Telecommunications markets. The high ' +
        'reliability XO ranges are resistant to shock and vibration in harsh environments and the specification ' +
        'profile makes it a great solution for Space and Defence applications.',
      imgUrl: './assets/backend-assets/prod-catgeory/clock-oscillators.png',
      path: PATH.CLK_OSC
    },
  ]

  constructor() {
  }

  /**
   * Return the {@link ProductType} associated the given url segment.
   *
   * @summary if more than one object is associated with a given segment, only the first match is returned!
   */
  async getProductType(segment: String): Promise<ProductType|undefined> {
    return await this.getProductTypes().then(
      types => {
        return types.find(type => {
          // if the routed url is /products/clock-oscillators - then compare only the segment: clock-oscillators
          return lastSegment(type.path) === segment
        })
      }
    )
  }

  async getProductTypes(): Promise<ProductType[]> {
    // TODO: Replace dummy with actual impl
    return Promise.resolve(this.productTypeList)
  }
}
