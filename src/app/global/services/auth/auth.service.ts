import {Injectable} from '@angular/core';
import {User} from "../../models";

export const AUTH_JWT = 'AuthJWT-iqd'

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private userObj: User = Object.freeze({email: 'joe.bloggs@gmail.com'})
  // private userInstance: User | null = null

  // Store the interrupted URL so, after sign in, it may be redirected to.
  redirectUrl: string | null = null

  constructor() {
  }

  get user(): User | null {
    return this.getJWT()
  }

  /**
   * Returns
   * TODO: Replace dummy sign-in with Backend call
   */
  async signIn(email: string, password: string): Promise<User> {
    return await new Promise(((resolve) => {
      setTimeout(() => {
        this.setJWT(this.userObj)
        resolve(this.userObj)
      }, 1000)
    }))
  }

  signOut() {
    this.clearJWT()
  }

  //region TODO: Move these to a JWTService in-line with Open-Closed in SOLID

  setJWT(token: User) {
    localStorage.setItem(AUTH_JWT, JSON.stringify(token))
  }

  private getJWT(): User|null {
    const value = localStorage.getItem(AUTH_JWT)
    return value ? JSON.parse(value) : null
  }

  private clearJWT() {
    localStorage.clear()
  }

  //endregion
}
