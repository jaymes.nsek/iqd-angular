// SEGMENTS
import {ActivatedRoute} from "@angular/router";

export const SEG = {
  SIGN_IN: 'sign-in',
  REGISTER: 'register',
  PASSWORD_RESET: 'reset',
  QUARTZ_CRY: 'quartz-crystal',
  CLK_OSC: 'clock-oscillators'
}


// PATHS
export const PATH = {
  SIGN_IN: `/auth/${SEG.SIGN_IN}`,
  REGISTER: `/auth/${SEG.REGISTER}`,
  PASSWORD_RESET: `/auth/${SEG.PASSWORD_RESET}`,
  MY_ACCOUNT: '/account',
  PRODUCTS: '/products',
  QUARTZ_CRY: `/products/${SEG.QUARTZ_CRY}`,
  CLK_OSC: `/products/${SEG.CLK_OSC}`
}


/**
 * Returns the last segment of the given url; or the string arg, if no segment is present.
 *
 * @param url e.g. /first-component/child-a/sub-child-a
 * @summary if only one segment exists in url, e.g. /first-component, first and last segment as equal
 */
export function lastSegment(url: string) {
  const lastSlash = url.lastIndexOf('/')
  return url.substring(lastSlash + 1)
}

/**
 * Returns the first segment, without the first char -expected to be '/', of the given url.
 *
 * @param url e.g. /first-component/child-a/sub-child-a
 * @summary if only one segment exists in url, e.g. /first-component, first and last segment as equal
 */
export function firstSegment(url: string) {
  // The second slash is taken as end index, if -1, i.e. not found, set as url len
  let endIndex = url.indexOf('/', 1)
  endIndex = endIndex < 1 ? url.length : endIndex

  // if url starts with "/", omit the first char in the return str
  const startIndex = url.charAt(0) == '/' ? 1 : 0
  return url.substring(startIndex, endIndex)
}

/**
 * Returns the ActivatedRoute path.
 *
 * @example if the parent url is /products/clock-oscillators as per the routing module setup,
 * the activated path, returned, is only: clock-oscillators
 */
export function getARUrlSegment(ar: ActivatedRoute) {
    return ar.snapshot.url[0].path
}
