import {firstSegment, lastSegment} from "./url-contants";

describe('Url Constants Helper',  () => {

  describe('#firstSegment', () => {
    it('with multiple segments: should return the first segment',  () => {
      const seg = firstSegment('/auth/reset')
      expect(seg).toBe('auth')
    })

    it('with a single segments: should return the first segment',  () => {
      const seg = firstSegment('/reset')
      expect(seg).toBe('reset')
    })

    it('with no segment: should return the string arg',  () => {
      const seg = firstSegment('no-segment')
      expect(seg).toBe('no-segment')
    })
  })

  describe('#lastSegment', () => {
    it('with multiple segments: should return the last segment',  () => {
      const seg = lastSegment('/auth/reset')
      expect(seg).toBe('reset')
    })

    it('with a single segments: should return the last segment',  () => {
      const seg = lastSegment('/reset')
      expect(seg).toBe('reset')
    })

    it('with no segment: should return the string arg',  () => {
      const seg = lastSegment('no-segment')
      expect(seg).toBe('no-segment')
    })
  })

})
