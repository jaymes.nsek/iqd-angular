// The goal of this test is to ensure that Validator.required only takes effect on submit btn as expected.
// For SIGN_IN and REGISTER states
//  - applies to email and password inputs
// For RESET password state
//  - applies only to email input

const VALID_EMAIL = 'anne.marie@gmail.com'
const VALID_PW = 'AqUa4#'

const INVALID_EMAIL = 'invalid_email.com'
const INVALID_PW = 'notValid1' // Missing symbol

/**
 * Addresses the test's blind spot, ensuring that omission of error elements,
 * due to code refactoring - for example, are flagged.
 *
 * @summary since test is only interested in confirming the presence of error
 * elements, a pass result for '/auth/sign-in' is deemed to satisfy other auth urls
 */
describe('Blind-spot verification test', () => {
  beforeEach(() => {
    cy.visit('/auth/sign-in')
  })

  describe('empty credentials', () => {
    it('should NOT display error message for empty [email]', () => {
      cy.get('div[id="email-wrap"]').within(() => {
        cy.get('input[id="email"]')
          .should('have.value', '')

        cy.get('#emailErr') // assert email error element does NOT exists
          .should('not.exist')
      })
    })

    it('should NOT display error message for empty [password]', () => {
      cy.get('div[id="password-wrap"]').within(() => {
        cy.get('input[id="password"]')
          .should('have.value', '')

        cy.get('#pwErr') // assert email error element does NOT exists
          .should('not.exist')
      })
    })
  })

  describe('valid credentials', () => {
    it('should NOT display error message for VALID [email]', () => {
      cy.get('div[id="email-wrap"]').within(() => {
        cy.get('input[id="email"]')
          .type(VALID_EMAIL)

        cy.get('#emailErr') // assert email error element does NOT exists
          .should('not.exist')
      })
    })

    it('should NOT display error message for VALID [password]', () => {
      cy.get('div[id="password-wrap"]').within(() => {
        cy.get('input[id="password"]')
          .type(VALID_PW)

        cy.get('#pwErr') // assert email error element does NOT exists
          .should('not.exist')
      })
    })
  })

  describe('invalid credentials', () => {
    it('should display error message for invalid [email]', () => {
      // email wrapper
      cy.get('div[id="email-wrap"]').within(() => {
        cy.get('input[id="email"]') // set text on email input
          .type(INVALID_EMAIL)

        cy.get('#emailErr') // assert email error element exists
          .should('have.class', 'err-feedback')
          .should('not.be.disabled')
          .should('have.text', 'invalid email')
      })
    })

    it('should display error message for invalid [password]', () => {
      // password wrapper
      cy.get('div[id="password-wrap"]').within(() => {
        cy.get('input[id="password"]') // set text on pw input
          .type('invalid_password')

        cy.get('#pwErr') // assert pw error element exists
          .should('have.class', 'err-feedback')
          .should('not.be.disabled')
          .should('have.text',
            'password requires at least 6 characters, containing uppercase, lowercase, number and symbol')
      })
    })
  })
})

const params = ['/auth/sign-in', '/auth/register']
params.forEach(url => {
  describe(`Submit button [${url}]`, () => {
    beforeEach(() => {
      cy.visit(url)
    })

    it('should DISABLE submit btn [empty email and password]', () => {
      // empty text fields for email and pw
      cy.get('div[id="email-wrap"] > input[id="email"]')
        .should('have.value', '')
      cy.get('div[id="password-wrap"] > input[id="password"]')
        .should('have.value', '')

      // Submit button is deactivated
      cy.get('form[id="main"] > button[id="submit-btn"]')
        .should('be.disabled')
    })

    it('should DISABLED submit btn [valid email (with invalid password)]', () => {
      cy.get('form[id="main"]').within(() => {
        cy.get('input[id="email"]')
          .type(VALID_EMAIL)

        cy.get('input[id="password"]')
          .type(INVALID_PW)

        cy.get('button[id="submit-btn"]') // Submit button is deactivated
          .should('be.disabled')
      })
    })

    it('should DISABLED submit btn [valid password (with invalid email)]', () => {
      cy.get('form[id="main"]').within(() => {
        cy.get('input[id="email"]')
          .type(INVALID_EMAIL)

        cy.get('input[id="password"]')
          .type(VALID_PW)

        cy.get('button[id="submit-btn"]') // Submit button is deactivated
          .should('be.disabled')
      })
    })

    it('should ENABLED submit btn [valid email and password]', () => {
      cy.get('form[id="main"]').within(() => {
        cy.get('input[id="email"]')
          .type(VALID_EMAIL)
        cy.get('input[id="password"]')
          .type(VALID_PW)

        cy.get('button[id="submit-btn"]') // Submit button is deactivated
          .should('be.enabled')
      })
    })
  })
})

const resetPath = '/auth/reset'
describe(`Submit button [${resetPath}]`, () => {
  beforeEach(() => {
    cy.visit(resetPath)
  })

  function passwordNotRendered() {
    // password field should not render
    cy.get('div#password-wrap')
      .should('have.css', 'display', 'none')
    cy.get('input#password')
      .should('not.be.visible')
  }

  it('should DISABLE submit btn [empty email]', () => {
    cy.get('form[id="main"]').within(() => {
      // empty text fields for email
      cy.get('input[id="email"]')
        .should('have.value', '')

      passwordNotRendered()

      cy.get('button#submit-btn') // Submit button is deactivated
        .should('be.disabled')
    })
  })

  it('should ENABLED submit btn [valid email]', () => {
    cy.get('form[id="main"]').within(() => {
      cy.get('input[id="email"]')
        .type(VALID_EMAIL)

      passwordNotRendered()

      cy.get('button[id="submit-btn"]') // Submit button is activated
        .should('be.enabled')
    })
  })
})
