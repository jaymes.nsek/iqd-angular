// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts
exports.config = {
  capabilities: {
    browserName: 'chrome'
  },
  directConnect: true,
  baseUrl: './',
  framework: 'jasmine',
  specs: [
    './e2e/**/*.e2e-spec.ts'
  ],

  // Options to pass onto Jasmine
  jasmineNodeOpts: {
    defaultTimeoutInterval: 10000
  },

  beforeLaunch: function() {
    require('ts-node').register({
      project: 'e2e/tsconfig.e2e.json'
    });
  }
};
