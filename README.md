# IqdAngular

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.1.3.

## Figma Mockup

I believe that prior (and proper planning) before coding makes for a pleasant coding experience. In addition to algorithm planning, I also undertake UI scaling where appropriate; design can be found at: 
<a href="https://www.figma.com/file/PkWzSseoqr2e5pI2RJnD11/design?node-id=0%3A1">Figma file<a>

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit and integration tests

Run `npm run test` to execute the unit and integration tests, with coverage, via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

For illustrative purpose, both Protractor, which recently reached end of life, and Cypress are used for the project.

To execute the end-to-end tests: 

Run `npm run pro:e2e` for Protractor

Run `npm run cy:e2e` for Cypress

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
